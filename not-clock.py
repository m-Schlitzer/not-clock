import random
import time

class not_time:
    def __init__(self):
        self.hour = random.randrange(0, 24)
        self.minutes = random.randrange(0, 60)

not_time_object = not_time()
time = time.localtime(time.time())
while not_time_object.hour == time.tm_hour:
    not_time_object.hour = random.randrange(0, 24)
while  not_time_object.minutes == time.tm_min:
    not_time_object.minutes = random.randrange(0, 60)
print('It\'s not %s:%s!' % (not_time_object.hour, not_time_object.minutes))
